package com.uca.gui;

import com.uca.core.PokemonCore;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;


public class PokemonGUI {
    //Plus utilisé
    public static String getPokemonByUser(int id) throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();

        Map<String, Object> input = new HashMap<>();

        input.put("pokemons", PokemonCore.getPokemonByUser(id));

        Writer output = new StringWriter();
        Template template = configuration.getTemplate("users/poketest.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(input, output);

        return output.toString();
    }

    //Affiche la page montrant le nouveau pokemon obtenu lors de la premiere connexion journalière
    public static String getNewPokemon(int userId) throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();

        Map<String, Object> input = new HashMap<>();

        input.put("pokemon", PokemonCore.getNewPokemon(userId));

        Writer output = new StringWriter();
        Template template = configuration.getTemplate("pokemons/newPokemon.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(input, output);

        return output.toString();
    }

    //Affiche l'interface permettant de choisir un pokemon a échangé dans un échange défini
    public static String getPokemonForNewExchangeOffer(int userId1, int useriId2, int pkmId) throws IOException, TemplateException
    {
        Configuration configuration = _FreeMarkerInitializer.getContext();

        Map<String, Object> input = new HashMap<>();


        input.put("pokemons", PokemonCore.getPokemonForExchange(useriId2, 0,0,false));
        input.put("pkm1", pkmId);
        input.put("user1", userId1);


        Writer output = new StringWriter();
        Template template = configuration.getTemplate("pokemons/pokemonForNewExchange.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(input, output);

        return output.toString();
    }

    //Affiche les pokemon selectionnable lorsque l'on essaye de répondre a une offre d'échange indéfini
    public static String getPokemonForExchange(int exchange, int onwerId, int id, int lvl, Boolean shiny) throws IOException, TemplateException
    {
        Configuration configuration = _FreeMarkerInitializer.getContext();

        Map<String, Object> input = new HashMap<>();


        input.put("pokemons", PokemonCore.getPokemonForExchange(onwerId, id, lvl, shiny));
        input.put("exchange", exchange);


        Writer output = new StringWriter();
        Template template = configuration.getTemplate("pokemons/pokemonForExchange.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(input, output);

        return output.toString();
    }
}
