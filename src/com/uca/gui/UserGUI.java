package com.uca.gui;

import com.uca.core.UserCore;
import com.uca.core.PokemonCore;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;


import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

public class UserGUI {

    //Plus utilisé
    public static String getAllUsers() throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();

        Map<String, Object> input = new HashMap<>();
        input.put("users", UserCore.getAllUsers());


        Writer output = new StringWriter();
        Template template = configuration.getTemplate("users/users.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(input, output);

        return output.toString();
    }

    //Renvoie la liste des joueur ayant pour id ou pseudo search
    public static String searchUser(String search) throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();
        Map<String, Object> input = new HashMap<>();
        input.put("users", UserCore.userSearch(search));


        Writer output = new StringWriter();
        Template template = configuration.getTemplate("users/searchUsers.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(input, output);

        return output.toString();
    }

    //Renvoie le profile d'un utilisateur différent de celui connecté
    public static String getUserById(int id) throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();

        Map<String, Object> input = new HashMap<>();
        input.put("user", UserCore.getUserById(id));
        input.put("pokemons", PokemonCore.getPokemonByUser(id));
        input.put("userinfo", UserCore.getUserInfo(id));

        Writer output = new StringWriter();
        Template template = configuration.getTemplate("users/profile.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(input, output);

        return output.toString();
    }

    //Renvoie le profil de l'utilisateur connecrté
    public static String getMyProfileById(int id) throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();

        Map<String, Object> input = new HashMap<>();
        input.put("user", UserCore.getUserById(id));
        input.put("pokemons", PokemonCore.getPokemonByUser(id));
        input.put("userinfo", UserCore.getUserInfo(id));

        Writer output = new StringWriter();
        Template template = configuration.getTemplate("users/myprofile.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(input, output);

        return output.toString();
    }
}
