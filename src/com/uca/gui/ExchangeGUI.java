package com.uca.gui;

import com.uca.core.ExchangeCore;
import com.uca.core.PokemonCore;
import com.uca.core.UserCore;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

public class ExchangeGUI {
    //Plus utilisé
    public static String getAllExchange() throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();

        Map<String, Object> input = new HashMap<>();

        input.put("exchanges", ExchangeCore.getAllExchange());
        

        Writer output = new StringWriter();
        Template template = configuration.getTemplate("exchanges/allExchange.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(input, output);

        return output.toString();
    }

    //Renvoie un échange
    public static String getExchangeById(int id) throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();

        Map<String, Object> input = new HashMap<>();

        input.put("exchange", ExchangeCore.getExchangeById(id));
        

        Writer output = new StringWriter();
        Template template = configuration.getTemplate("exchanges/exchange.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(input, output);

        return output.toString();
    }

    //Renvoie une offre d'échange (choix d'un pokemon dans échange non défini)
    public static String getExchangeOffer(int id, int dataId) throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();

        Map<String, Object> input = new HashMap<>();


        input.put("exchange", ExchangeCore.getNewOffer(id, dataId));
        

        Writer output = new StringWriter();
        Template template = configuration.getTemplate("exchanges/exchangeOffer.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(input, output);

        return output.toString();
    }

    //Interface qui permet la creation d'un echange défini
    public static String newExchange(int iduser1, int dataId, int iduser2, int pkm2) throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();

        Map<String, Object> input = new HashMap<>();

        input.put("exchange", ExchangeCore.newExchangeOffer(iduser1, dataId,iduser2, pkm2));
        

        Writer output = new StringWriter();
        Template template = configuration.getTemplate("exchanges/newExchange.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(input, output);

        return output.toString();
    }

    //Affiche les echanges entrant d'un utilisateur
    public static String exchangeIn(int iduser) throws IOException, TemplateException
    {
        Configuration configuration = _FreeMarkerInitializer.getContext();

        Map<String, Object> input = new HashMap<>();

        input.put("exchanges", ExchangeCore.getAllExchangeIn(iduser));
        

        Writer output = new StringWriter();
        Template template = configuration.getTemplate("exchanges/allExchangeIn.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(input, output);

        return output.toString();
    }

    //Affiche les echanges sortant d'un utilisateur
    public static String exchangeOut(int iduser, boolean actualUser) throws IOException, TemplateException
    {
        Configuration configuration = _FreeMarkerInitializer.getContext();

        Map<String, Object> input = new HashMap<>();

        input.put("exchanges", ExchangeCore.getAllExchangeOut(iduser, actualUser));
        

        Writer output = new StringWriter();
        Template template;
        if(actualUser)
            template = configuration.getTemplate("exchanges/allMyExchangeOut.ftl");
        else
            template = configuration.getTemplate("exchanges/allExchangeOut.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(input, output);

        return output.toString();
    }

    //Affiche l'interface pour creer un échange indéfini
    public static String myNewExchange(int userId, int dataId) throws IOException, TemplateException
    {
        Configuration configuration = _FreeMarkerInitializer.getContext();

        Map<String, Object> input = new HashMap<>();

        input.put("user", UserCore.getUserById(userId));
        input.put("pokemon", PokemonCore.getPokemonByDataId(dataId));
        

        Writer output = new StringWriter();
        Template template = configuration.getTemplate("exchanges/myNewExchange.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(input, output);

        return output.toString();
    }
}