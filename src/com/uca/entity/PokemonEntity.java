package com.uca.entity;

import java.util.Date;
import java.sql.Timestamp;

import java.io.File;

public class PokemonEntity {
	private int id;
	private int lvl;
	private String name;
	//Non utilisé
	private String description;
	//Non utilisé
	private String[] types;
	private Date getting_date;
	private String gender;
	private Boolean shiny;
	private String sprite;
	private int base_owner;
	private int dataId;

	public PokemonEntity()
	{
		this.sprite = null;
		this.types = new String[2];
		this.types[0] = null;
		this.types[1] = null;
	}

	public PokemonEntity(int id, int lvl, Boolean shiny)
	{
		this.id = id;
		this.lvl = lvl;
		this.shiny = shiny;
	}

	public PokemonEntity(int id, String name, int lvl, Boolean shiny)
	{

		this.id = id;
		this.lvl = lvl;
		if(name != "")
			name = name.substring(0,1).toUpperCase() + name.substring(1);
		this.name = name; 
		this.shiny = shiny;
		this.gender = "";

		this.sprite = shiny ? "/pokemonSprite/shiny/"+String.valueOf(id)+".png" : "/pokemonSprite/"+String.valueOf(id)+".png";

		this.types = new String[2];
		this.types[0] = null;
		this.types[1] = null;
	}

	public int getId()
	{
		return this.id;
	}

	public void setId(int id)
	{
		if(id < 0)
			throw new IllegalArgumentException("Un index ne peut pas etre négatif");
		this.id = id;
	}

	public int getDataId()
	{
		return this.dataId;
	}

	public void setDataId(int data)
	{
		this.dataId = data;
	}

	public int getLvl()
	{
		return this.lvl;
	}

	public void setLvl(int lvl)
	{
		this.lvl = lvl;
	}

	public String getName()
	{
		String str = new String(this.name);
		return str;
	}

	public void setName(String name)
	{
		name = name.substring(0,1).toUpperCase() + name.substring(1);
		this.name = name;
	}

	public String getDescription()
	{
		String str = new String(this.description);
		return str;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getType(int i)
	{
		if(i < 0 || i > 2)
			throw new IndexOutOfBoundsException();
		String str = new String(this.types[i]);
		return str;
	}

	public void setType(int i, String type)
	{
		if(i < 0 || i > 2)
			throw new IndexOutOfBoundsException();
		this.types[i] = type;
	}

	public Date getGettingDate()
	{
		return this.getting_date;
	}

	public void setGettingDate(Date date)
	{
		this.getting_date = date;
	}

	public String getGender()
	{
		String str = new String(this.gender);
		return str;
	}

	public void setGender(String gender)
	{
		this.gender = gender;
	}

	public Boolean getShiny()
	{
		Boolean bool = new Boolean(this.shiny);
		return bool;
	}

	public void setShiny(Boolean shiny)
	{
		this.shiny = shiny;
	}

	public String getSprite()
	{
		String str = new String(this.sprite);
		return str;
	}

	public void setSprite(String sprite)
	{
		this.sprite = sprite;
	}

	public int getBaseOwner()
	{
		return this.base_owner;
	}

	public void setBaseOwner(int id)
	{
		if(id < 0)
			throw new IllegalArgumentException("Un index ne peut pas etre négatif");
		this.base_owner = id;
	}

	public String toString()
	{
		return this.name + " id: " + String.valueOf(this.id) + " lvl: " + String.valueOf(this.lvl) + " shiny: " + String.valueOf(this.shiny) + " dataId: " + String.valueOf(this.dataId);
	}

	public PokemonEntity clone()
	{
		PokemonEntity clone = new PokemonEntity();
		clone.setId(this.id);
		clone.setName(this.getName());
		clone.setLvl(this.lvl);
		clone.setDescription(this.getDescription());
		clone.setBaseOwner(this.base_owner);
		clone.setSprite(this.getSprite());
		clone.setShiny(this.getShiny());
		clone.setGender(this.getGender());
		clone.setGettingDate(this.getting_date);
		clone.setDataId(this.dataId);
		clone.setType(0, this.getType(0));
		clone.setType(1, this.getType(1));

		return clone;
	}

}