package com.uca.entity;

//Contient des infos propres a un utlisateur
public class UserInfosEntity extends UserEntity{
    private int distinctPkm;
    private int pkm;
    private int shiny;

    public UserInfosEntity() {
        //Ignored !
    }

    public int getDistinctPkm()
    {
    	return this.distinctPkm;
    }

    public void setDistinctPkm(int distinctPkm)
    {
    	this.distinctPkm = distinctPkm;
    }

    public int getPkm()
    {
    	return this.pkm;
    }

    public void setPkm(int pkm)
    {
    	this.pkm = pkm;
    }

    public int getShiny()
    {
    	return this.shiny;
    }

    public void setShiny(int shiny)
    {
    	this.shiny = shiny;
    }

}