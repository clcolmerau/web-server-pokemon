package com.uca.entity;

import java.util.Date;
import java.sql.Timestamp;

public class UserEntity {
    private int login;
    private String pseudo;
    private String email;
    private String hashpswd;
    private String avatarURL;
    private int points;
    private Date last_co_date;

    public UserEntity() {
        //Ignored !
    }

    public UserEntity(String pseudo, String pswd, String email) {
        this.pseudo = pseudo;
        this.email = email;
        this.hashpswd = pswd;
    }

    public int getLogin() {
        return this.login;
    }

    public void setLogin(int login) {
        this.login = login;
    }

    public String getPseudo() {
        return this.pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHashpswd() {
        return this.hashpswd;
    }

    public void setHashpswd(String pswd) {
        this.hashpswd = pswd;
    }

    public Date getLastCoDate()
    {
        return this.last_co_date;
    }

    public void setLastCoDate(Date date)
    {
        this.last_co_date = date;
    }
}
