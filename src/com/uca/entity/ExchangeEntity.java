package com.uca.entity;

public class ExchangeEntity {
    private int id;
    private UserEntity user1;
    //Id dans la table own
    private PokemonEntity pkm1;
    private UserEntity user2;
    private PokemonEntity idPkm2;
    //Id dans la table own
    private PokemonEntity dataPkm2;


    public ExchangeEntity() {
        //
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public PokemonEntity getPkm1() {
        return this.pkm1;
    }

    public void setPkm1(PokemonEntity pkm1) {
        this.pkm1 = pkm1;
    }

    public PokemonEntity getDataPkm2() {
        return this.dataPkm2;
    }

    public void setDataPkm2(PokemonEntity dataPkm2) {
        this.dataPkm2 = dataPkm2;
    }

    public PokemonEntity getIdPkm2() {
        return this.idPkm2;
    }

    public void setIdPkm2(PokemonEntity idPkm2) {
        this.idPkm2 = idPkm2;
    }

    public UserEntity getUser1()
    {
        return this.user1;
    }

    public void setUser1(UserEntity user)
    {
        this.user1 = user;
    }

    public UserEntity getUser2()
    {
        return this.user2;
    }

    public void setUser2(UserEntity user)
    {
        this.user2 = user;
    }

    public String toString()
    {

    	return "Exchange : id = " + String.valueOf(this.id)  + String.valueOf(this.pkm1) + " pkm2 = " + String.valueOf(this.idPkm2) + " dataidPkm2 = " + String.valueOf(this.dataPkm2);
    }

    public ExchangeEntity clone()
    {
        ExchangeEntity clone = new ExchangeEntity();
        clone.setId(this.id);
        clone.setPkm1(this.pkm1);
        clone.setDataPkm2(this.dataPkm2);
        clone.setIdPkm2(this.dataPkm2);

        return clone;
    }
}