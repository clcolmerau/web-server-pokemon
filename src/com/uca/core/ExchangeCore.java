package com.uca.core;

import com.uca.dao.ExchangeDAO;
import com.uca.entity.ExchangeEntity;
import com.uca.entity.PokemonEntity;
import com.uca.entity.UserEntity;

import java.io.IOException;

import java.util.ArrayList;

public class ExchangeCore {

    //Renvoie tout les échange
    public static ArrayList<ExchangeEntity> getAllExchange() throws IOException {
        return new ExchangeDAO().getAllExchange();
    }

    //Renvoie l'échange cible
    public static ExchangeEntity getExchangeById(int exchangeId) throws IOException
    {
        return new ExchangeDAO().getExchangeById(exchangeId);
    }

    //Renvoie une offre d'échange avec le pokemon selectionné dans le cas d'un échange indéfini 
    public static ExchangeEntity getNewOffer(int id, int dataId) throws IOException
    {
        ExchangeEntity entity = new ExchangeDAO().getExchangeById(id);

        if(entity != null)
        {
            entity.setDataPkm2(PokemonCore.getPokemonByDataId(dataId));
        }
        return entity;
    }

    //Effectue un échange
    public static Boolean doExchange(ExchangeEntity exchange) throws IOException
    {
        return new ExchangeDAO().doExchange(exchange);
    }

    //Effectue un échange
    public static Boolean doExchange(int exchange) throws IOException
    {
        return new ExchangeDAO().doExchange(exchange);
    }

    //Effectue un échange
    public static Boolean doExchange(int exchange, int dataId) throws IOException
    {
        ExchangeEntity entity = getExchangeById(exchange);
        entity.setDataPkm2(PokemonCore.getPokemonByDataId(dataId));
        return doExchange(entity);
    }

    //Effectue un échange
    public static Boolean doExchange(int exchange, int dataId, int idUser2) throws IOException
    {
        ExchangeEntity entity = getExchangeById(exchange);
        entity.setDataPkm2(PokemonCore.getPokemonByDataId(dataId));
        entity.setUser2(UserCore.getUserById(idUser2));
        return doExchange(entity);
    }

    //Creer une offre d'change (n'update pas la BDD)
    public static ExchangeEntity newExchangeOffer(int iduser1, int dataId, int iduser2, int idpkm2) throws IOException
    {
        PokemonEntity pkm1 = PokemonCore.getPokemonByDataId(dataId);
        ExchangeEntity exchange = new ExchangeEntity();
        UserEntity user1 = UserCore.getUserById(iduser1);
        UserEntity user2 = UserCore.getUserById(iduser2);
        if(user2 == null || user1 == null)
            return null;
        exchange.setUser1(user1);
        exchange.setPkm1(pkm1);
        if(idpkm2 == 0)
            exchange.setDataPkm2(null);
        else
            exchange.setDataPkm2(PokemonCore.getPokemonByDataId(idpkm2));
        exchange.setIdPkm2(null);
        exchange.setUser2(user2);
        return exchange;
    }

    //Renvoie tout les échange entrant d'un utilisateur
    public static ArrayList<ExchangeEntity> getAllExchangeIn(int userId) throws IOException
    {
        return new ExchangeDAO().getAllExchangeIn(userId);
    }

    //Renvoie tout les échange sortant d'un utilisateur
    public static ArrayList<ExchangeEntity> getAllExchangeOut(int userId, boolean actualUser) throws IOException
    {
        if(actualUser)
            return new ExchangeDAO().getAllMyExchangeOut(userId);
        else
            return new ExchangeDAO().getAllExchangeOut(userId);
    }

    //Creer un nouvel echange
    public static int createNewExchange(int userId, int dataId, String pkmName, int pkmLvl, Boolean shiny) throws IOException
    {
        if(!PokemonCore.pokemonExist(pkmName))
            return 2;
        if(pkmLvl < 0 || pkmLvl >100)
            return 3;
        PokemonEntity pkm2 = new PokemonEntity(pkmName.isEmpty() ? 0 : PokemonCore.getPokemonIdByName(pkmName), pkmLvl , shiny);
        PokemonEntity pkm1 = PokemonCore.getPokemonByDataId(dataId);
        ExchangeEntity exchange = new ExchangeEntity();
        UserEntity user1 = UserCore.getUserById(userId);
        if(pkm2 == null || user1 == null)
            return 0;
        exchange.setUser1(user1);
        exchange.setPkm1(pkm1);
        exchange.setDataPkm2(null);
        exchange.setIdPkm2(pkm2);
        exchange.setUser2(null);
        exchange = new ExchangeDAO().create(exchange);

        return exchange == null ? 0 : 1;
    }

    //Creer un nouvel echange
    public static int createNewExchange(int userId, int dataId, int iduser2, int idpkm2) throws IOException
    {
        PokemonEntity pkm2 = PokemonCore.getPokemonByDataId(idpkm2);
        PokemonEntity pkm1 = PokemonCore.getPokemonByDataId(dataId);
        ExchangeEntity exchange = new ExchangeEntity();
        UserEntity user1 = UserCore.getUserById(userId);
        UserEntity user2 = UserCore.getUserById(iduser2);
        if(pkm2 == null || user1 == null || pkm2 == null || user2 == null)
            return 0;
        exchange.setUser1(user1);
        exchange.setPkm1(pkm1);
        exchange.setDataPkm2(pkm2);
        exchange.setIdPkm2(null);
        exchange.setUser2(user2);
        
        exchange = new ExchangeDAO().create(exchange);
        return exchange == null ? 0 : 1;
    }

    //Refuse un échange
    public static Boolean refuseExchange(int id) throws IOException
    {
        ExchangeEntity exchange = getExchangeById(id);
        if(exchange == null)
            return false;
        new ExchangeDAO().delete(exchange);
        return true;

    }

    //Vérifie si un échange existe
    public static int exchangeExist(int id) throws IOException
    {
        Boolean bool = new ExchangeDAO().exchangeExist(id);
        //Existe
        if(bool)
            return 1;
        //N'éxiste pas
        if(!bool)
            return 2;
        else
            return 0;
        //Erreur
    }

}
