package com.uca.core;

import com.uca.dao.PokemonDAO;
import com.uca.entity.PokemonEntity;

import java.io.IOException;

import java.util.ArrayList;

public class PokemonCore {

    //Renvoie le pokemon avec l'id ciblé (pokeapi)
    public static PokemonEntity getPokemonById(int id) throws IOException {
        return new PokemonDAO().getPokemonById(id);
    }

    //Renvoie l'id du pokemon avec le nom donné (pokeapi)
    public static int getPokemonIdByName(String name) throws IOException
    {
        return new PokemonDAO().getPokemonIdByName(name);
    }

    //Réciroque de la prédcédente (pokeapi)
    public static String getPokemonNameById(int id) throws IOException {
        return new PokemonDAO().getPokemonNameById(id);
    }

    //Renvoie tout les pokemon d'un utilisateur
    public static ArrayList<PokemonEntity> getPokemonByUser(int id) throws IOException{
        return new PokemonDAO().getPokemonByUser(id);
    }

    //Permet de tirer au hasard un pokemon
    public static PokemonEntity getNewPokemon(int userId) throws IOException{
        return new PokemonDAO().getNewPokemon(userId);
    }

    //Ajoute un niveau au pokemon cible
    public static int lvlUpPokemonById(int id)
    {
       return new PokemonDAO().lvlUpPokemonById(id);

    }

    //Renvoie une instance simplifié d'un pokemon 
    public static PokemonEntity getSimpleInstance(int id, int lvl, Boolean shiny) throws IOException
    {
        PokemonEntity pkm = new PokemonEntity(id, id == 0 ? "" : getPokemonNameById(id), lvl ,shiny);
        if(pkm.getSprite() == null)
            PokemonDAO.setPokemonSprite(pkm);
        return pkm;
    }

    //Renvoie l'id de l'utilisateur ayant le pokemon cible
    public static int getOwnerId(int pkmId) throws IOException
    {
        return new PokemonDAO().getOwnerId(pkmId);
    }

    //Renvoie un pokemon de la table own
    public static PokemonEntity getPokemonByDataId(int id) throws IOException
    {
        return new PokemonDAO().getPokemonByDataId(id);
    }

    //Permet de selectionner un pokemon pour un echange
    public static ArrayList<PokemonEntity> getPokemonForExchange(int ownerId, int id, int lvl, Boolean shiny) throws IOException
    {
        PokemonEntity pkm = new PokemonEntity(id, lvl, shiny);
        return new PokemonDAO().getPokemonForExchange(ownerId, pkm);
    }

    //Vérifie si un pokemon existe dans notre BDD
    public static Boolean pokemonExist(String name) throws IOException
    {
        return new PokemonDAO().pokemonExist(name);
    }

    //Vérifie si un pokemon existe dans notre BDD
    public static Boolean pokemonExist(int id) throws IOException
    {
        return new PokemonDAO().pokemonExist(id);
    }

    //Vérifie si un pokemon appartient a un utilisateur
    public static Boolean isPokemonOwnedByUser(int userId, int pkmId) throws IOException
    {
        return new PokemonDAO().isPokemonOwnedByUser(userId,pkmId);
    }

}
