package com.uca.core;

import com.uca.dao.UserDAO;
import com.uca.entity.UserEntity;
import com.uca.entity.UserInfosEntity;


import java.util.ArrayList;

public class UserCore {

    //Renvoie tout les utilsateur
    public static ArrayList<UserEntity> getAllUsers() {
        return new UserDAO().getAllUsers();
    }

    //Renvoie l'utilisateur ciblé
    public static UserEntity getUserById(int id) {
        return new UserDAO().getUserById(id);
    }

    //Vérifie si un mail est enregistré
    public static Boolean userEmailExist(String mail){
        return new UserDAO().userEmailExist(mail);
    }

    //Crée un nouvel utilisateur
    public static UserEntity createUser(String pseudo, String pswd, String email)
    {
        UserEntity user = new UserEntity(pseudo, pswd, email);
        return new UserDAO().create(user);
    }

    //Récupère les infos d'un utilisateur (pour le profil)
    public static UserInfosEntity getUserInfo(int id)
    {
        return new UserDAO().getUserInfo(id);
    }

    //Recupère l'id d'un utilisateur avec le mail donné
    public static int getUserIdByMail(String mail)
    {
        return new UserDAO().getUserIdByMail(mail);
    }

    //Test si les infos de login sont bonnes
    public static int login(String pswd, String mail)
    {
        return new UserDAO().login(pswd, mail);
    }

    //Test les lvlup d'un joueur 
    public static Boolean lvlup(int id)
    {
        return new UserDAO().lvlup(id);
    }

    //Met a jour les lvlup des joueur a 5
    public static void updateLvlUp()
    {
        new UserDAO().updateLvlUp();
    }

    //Renvoie tout les utilisateur ayant le login ou le pseaudo valant search
    public static ArrayList<UserEntity> userSearch(String search) 
    {
        return new UserDAO().userSearch(search);
    }

    //Vérifie si un utilisateur existe
    public static int userExist(int id)
    {
        Boolean bool = new UserDAO().userExist(id);
        if(bool)
            return 1;
        if(!bool)
            return 2;
        return 0;
    }
}
