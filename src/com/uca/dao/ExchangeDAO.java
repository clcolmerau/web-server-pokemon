package com.uca.dao;

import com.uca.entity.ExchangeEntity;
import com.uca.core.PokemonCore;
import com.uca.core.UserCore;
import com.uca.entity.PokemonEntity;
import org.mindrot.jbcrypt.BCrypt;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Calendar;
import java.io.IOException;

public class ExchangeDAO extends _Generic<ExchangeEntity> {
    //Renvoie tout les échange (plus utilisé)
	public ArrayList<ExchangeEntity> getAllExchange() throws IOException
	{
		ArrayList<ExchangeEntity> entities = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement("SELECT * FROM exchange ORDER BY id ASC;");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                ExchangeEntity entity = new ExchangeEntity();
                entity.setId(resultSet.getInt("id"));
                entity.setPkm1(PokemonCore.getPokemonByDataId(resultSet.getInt("idpkm1")));
                entity.setDataPkm2(resultSet.getInt("IDDATA_PKM2") != 0 ? PokemonCore.getPokemonByDataId(resultSet.getInt("IDDATA_PKM2")) : null);
                entity.setUser1(UserCore.getUserById(resultSet.getInt("iduser1")));
                entity.setUser2(resultSet.getInt("iduser2") != 0 ? UserCore.getUserById(resultSet.getInt("iduser2")) : null);

                if(entity.getDataPkm2() ==null)
                entity.setIdPkm2(PokemonCore.getSimpleInstance(resultSet.getInt("idpkm2"), resultSet.getInt("lvl") ,resultSet.getBoolean("shiny")));

                
                entities.add(entity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return entities;
	}

    //Vérifie si un echange existe
    public Boolean exchangeExist(int id) throws IOException
    {
        Boolean exist = null;
        try {

            PreparedStatement preparedStatement = this.connect.prepareStatement("SELECT * FROM exchange WHERE id = ? ;");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            exist =  resultSet.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return exist;
    }

    //Valide et effectu un échange
    public Boolean doExchange(ExchangeEntity exchange) throws IOException
    {
        try 
        {
            PreparedStatement preparedStatement = this.connect.prepareStatement("UPDATE own SET idowner = (?), getting_Date = (?) WHERE id = (?);");
            java.util.Date date = new java.util.Date();
            preparedStatement.setInt(1, exchange.getUser1().getLogin());
            preparedStatement.setDate(2, new java.sql.Date(date.getTime()));
            preparedStatement.setInt(3, exchange.getDataPkm2().getDataId());
            preparedStatement.executeUpdate();   

            preparedStatement = this.connect.prepareStatement("UPDATE own SET idowner = (?), getting_Date = (?) WHERE id = (?);");
            preparedStatement.setInt(1, exchange.getUser2().getLogin());
            preparedStatement.setDate(2, new java.sql.Date(date.getTime()));
            preparedStatement.setInt(3, exchange.getPkm1().getDataId());
            preparedStatement.executeUpdate();   

            //Supprime tout les échange contenant un des pokemon échangé (ils ne sont plus valide)
            preparedStatement = this.connect.prepareStatement("DELETE FROM exchange WHERE idpkm1 = (?) or IDDATA_PKM2 = (?);");
            preparedStatement.setInt(1, exchange.getPkm1().getDataId());
            preparedStatement.setInt(2, exchange.getDataPkm2().getDataId());
            preparedStatement.executeUpdate();

        
            return true;            
        } 
        catch (SQLException e) 
        {
            e.printStackTrace();
        }
        return false;
    }

    //Valide et effectu un échange
    public Boolean doExchange(int exchange) throws IOException
    {
        ExchangeEntity entity = getExchangeById(exchange);
        return doExchange(entity);

    }

    //Récupere un échange par un id
    public ExchangeEntity getExchangeById(int id) throws IOException
    {
        ExchangeEntity entity = null;
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement("SELECT * FROM exchange WHERE id = (?);");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()) {
                entity = new ExchangeEntity();
                entity.setId(resultSet.getInt("id"));
                entity.setPkm1(PokemonCore.getPokemonByDataId(resultSet.getInt("idpkm1")));
                entity.setDataPkm2(resultSet.getInt("IDDATA_PKM2") != 0 ? PokemonCore.getPokemonByDataId(resultSet.getInt("IDDATA_PKM2")) : null);
                entity.setUser1(UserCore.getUserById(resultSet.getInt("iduser1")));
                entity.setUser2(resultSet.getInt("iduser2") != 0 ? UserCore.getUserById(resultSet.getInt("iduser2")) : null);

                if(entity.getDataPkm2() ==null)
                    entity.setIdPkm2(PokemonCore.getSimpleInstance(resultSet.getInt("idpkm2"), resultSet.getInt("lvl") ,resultSet.getBoolean("shiny")));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return entity;
    }

    //Récupère tous les échange entrant d'un utilisateur
    public ArrayList<ExchangeEntity> getAllExchangeIn(int userId) throws IOException
    {
        ArrayList<ExchangeEntity> entities = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement("SELECT * FROM exchange where iduser2 = (?) ORDER BY id ASC;");
            preparedStatement.setInt(1,userId); 
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                ExchangeEntity entity = new ExchangeEntity();
                entity.setId(resultSet.getInt("id"));
                entity.setPkm1(PokemonCore.getPokemonByDataId(resultSet.getInt("idpkm1")));
                entity.setDataPkm2(resultSet.getInt("IDDATA_PKM2") != 0 ? PokemonCore.getPokemonByDataId(resultSet.getInt("IDDATA_PKM2")) : null);
                entity.setUser1(UserCore.getUserById(resultSet.getInt("iduser1")));
                entity.setUser2(resultSet.getInt("iduser2") != 0 ? UserCore.getUserById(resultSet.getInt("iduser2")) : null);

                if(entity.getDataPkm2() ==null)
                entity.setIdPkm2(PokemonCore.getSimpleInstance(resultSet.getInt("idpkm2"), resultSet.getInt("lvl") ,resultSet.getBoolean("shiny")));

                
                entities.add(entity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return entities;
    }

    //Récupère tous les échange sortants d'un utilisateur
    public ArrayList<ExchangeEntity> getAllExchangeOut(int userId) throws IOException
    {
        ArrayList<ExchangeEntity> entities = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement("SELECT * FROM exchange WHERE iduser1 = (?) ORDER BY id ASC;");
            preparedStatement.setInt(1,userId); 
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                if(resultSet.getInt("IDDATA_PKM2") == 0)
                {
                    ExchangeEntity entity = new ExchangeEntity();
                    entity.setId(resultSet.getInt("id"));
                    entity.setPkm1(PokemonCore.getPokemonByDataId(resultSet.getInt("idpkm1")));
                    entity.setUser1(UserCore.getUserById(resultSet.getInt("iduser1")));
                    entity.setUser2(resultSet.getInt("iduser2") != 0 ? UserCore.getUserById(resultSet.getInt("iduser2")) : null);

                    if(entity.getDataPkm2() ==null)
                    entity.setIdPkm2(PokemonCore.getSimpleInstance(resultSet.getInt("idpkm2"), resultSet.getInt("lvl") ,resultSet.getBoolean("shiny")));

                    
                    entities.add(entity);

                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return entities;
    }

    //Récupère tout les échange sortant de l'utilisateur connecté
    public ArrayList<ExchangeEntity> getAllMyExchangeOut(int userId) throws IOException
    {
        ArrayList<ExchangeEntity> entities = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement("SELECT * FROM exchange where iduser1 = (?) ORDER BY id ASC;");
            preparedStatement.setInt(1,userId); 
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                ExchangeEntity entity = new ExchangeEntity();
                entity.setId(resultSet.getInt("id"));
                entity.setPkm1(PokemonCore.getPokemonByDataId(resultSet.getInt("idpkm1")));
                entity.setDataPkm2(resultSet.getInt("IDDATA_PKM2") != 0 ? PokemonCore.getPokemonByDataId(resultSet.getInt("IDDATA_PKM2")) : null);
                entity.setUser1(UserCore.getUserById(resultSet.getInt("iduser1")));
                entity.setUser2(resultSet.getInt("iduser2") != 0 ? UserCore.getUserById(resultSet.getInt("iduser2")) : null);

                if(entity.getDataPkm2() ==null)
                entity.setIdPkm2(PokemonCore.getSimpleInstance(resultSet.getInt("idpkm2"), resultSet.getInt("lvl") ,resultSet.getBoolean("shiny")));

                
                entities.add(entity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return entities;
    }


	@Override
    public ExchangeEntity create(ExchangeEntity exchange) 
    {
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement("INSERT INTO exchange(iduser1, idpkm1, idpkm2, lvl, shiny, IDDATA_PKM2, iduser2) VALUES(?,?,?,?,?, ?,?);");
            preparedStatement.setInt(1, exchange.getUser1().getLogin());
            preparedStatement.setInt(2, exchange.getPkm1().getDataId());
            preparedStatement.setInt(3, exchange.getIdPkm2() == null ? 0 : exchange.getIdPkm2().getDataId());
            preparedStatement.setInt(4, exchange.getIdPkm2() == null ? 0 : exchange.getIdPkm2().getLvl());
            preparedStatement.setBoolean(5, exchange.getIdPkm2() == null ? false : exchange.getIdPkm2().getShiny());
            preparedStatement.setInt(6, exchange.getDataPkm2() == null ? 0 : exchange.getDataPkm2().getDataId());
            preparedStatement.setInt(7, exchange.getUser2() == null ? 0 : exchange.getUser2().getLogin());
            preparedStatement.executeUpdate();
            return exchange;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void delete(ExchangeEntity exchange) 
    {
        try 
        {
            PreparedStatement preparedStatement = this.connect.prepareStatement("DELETE FROM exchange WHERE id = (?);");
            preparedStatement.setInt(1, exchange.getId());
            preparedStatement.executeUpdate();           
        } 
        catch (SQLException e) 
        {
            e.printStackTrace();
        }
    }
}