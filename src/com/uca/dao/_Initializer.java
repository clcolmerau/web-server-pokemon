package com.uca.dao;

import java.sql.*;
import java.util.Date;
import com.uca.util.Scheduler;

import com.uca.core.UserCore;

public class _Initializer {

    public static void Init(){
        Connection connection = _Connector.getInstance();

        try {
            PreparedStatement statement;
            //Init articles table
            statement = connection.prepareStatement("CREATE TABLE IF NOT EXISTS users (login int primary key auto_increment, pseudo varchar(100), email varchar(100), salt varchar(50), hashpswd varchar(100), avatarURL varchar(100), last_date_co date, lvlup int); ");
            statement.executeUpdate();
            statement = connection.prepareStatement("CREATE TABLE IF NOT EXISTS OWN (ID INT PRIMARY KEY AUTO_INCREMENT, IDOWNER INT, IDPKM INT, GENDER VARCHAR(20), SHINY Boolean, LVL INT, BASEOWNER INT, GETTING_DATE DATE, CONSTRAINT FK_IDOWNER FOREIGN KEY (IDOWNER) REFERENCES USERS(LOGIN), CONSTRAINT FK_BASEOWNER FOREIGN KEY (BASEOWNER) REFERENCES USERS(LOGIN)); ");
            statement.executeUpdate();
            statement = connection.prepareStatement("CREATE TABLE IF NOT EXISTS exchange (ID INT PRIMARY KEY AUTO_INCREMENT, IDPKM1 INT, IDPKM2 INT, IDDATA_PKM2 INT, SHINY Boolean, LVL INT, iduser1 int, iduser2 int,CONSTRAINT FK_USER1 FOREIGN KEY (iduser1) REFERENCES users(login));");
            statement.executeUpdate();

            //Initialise BDD avec valeur test
            UserCore.createUser("foo", "pswd1","foomail");

            UserCore.createUser("toto", "pswd1", "totomail");

            UserCore.createUser("titi",  "pswd1", "titimail");

            statement = connection.prepareStatement("INSERT INTO own(idowner, idpkm, gender, shiny, lvl, baseowner, getting_date) VALUES(?, ? , ?, ?, ?, ?, ?);");
            statement.setInt(1, 1);
            statement.setInt(2, 431);
            statement.setString(3, "female");
            statement.setBoolean(4, true);
            statement.setInt(5, 23);
            statement.setInt(6, 1);
            statement.setDate(7, java.sql.Date.valueOf("2022-11-11"));
            statement.executeUpdate();

            statement = connection.prepareStatement("INSERT INTO own(idowner, idpkm, gender, shiny, lvl, baseowner, getting_date) VALUES(?, ? , ?, ?, ?, ?, ?);");
            statement.setInt(1, 1);
            statement.setInt(2, 20);
            statement.setString(3, "male");
            statement.setBoolean(4, false);
            statement.setInt(5, 10);
            statement.setInt(6, 1);
            statement.setDate(7, java.sql.Date.valueOf("2022-11-10"));
            statement.executeUpdate();

            statement = connection.prepareStatement("INSERT INTO own(idowner, idpkm, gender, shiny, lvl, baseowner, getting_date) VALUES(?, ? , ?, ?, ?, ?, ?);");
            statement.setInt(1, 2);
            statement.setInt(2, 719);
            statement.setString(3, "genderless");
            statement.setBoolean(4, false);
            statement.setInt(5, 13);
            statement.setInt(6, 2);
            statement.setDate(7, java.sql.Date.valueOf("2022-09-08"));
            statement.executeUpdate();

            statement = connection.prepareStatement("INSERT INTO own(idowner, idpkm, gender, shiny, lvl, baseowner, getting_date) VALUES(?, ? , ?, ?, ?, ?, ?);");
            statement.setInt(1, 2);
            statement.setInt(2, 745);
            statement.setString(3, "female");
            statement.setBoolean(4, false);
            statement.setInt(5, 32);
            statement.setInt(6, 3);
            statement.setDate(7, java.sql.Date.valueOf("2022-09-08"));
            statement.executeUpdate();

            statement = connection.prepareStatement("INSERT INTO own(idowner, idpkm, gender, shiny, lvl, baseowner, getting_date) VALUES(?, ? , ?, ?, ?, ?, ?);");
            statement.setInt(1, 3);
            statement.setInt(2, 14);
            statement.setString(3, "male");
            statement.setBoolean(4, false);
            statement.setInt(5, 85);
            statement.setInt(6, 2);
            statement.setDate(7, java.sql.Date.valueOf("2022-09-08"));
            statement.executeUpdate();

            statement = connection.prepareStatement("INSERT INTO own(idowner, idpkm, gender, shiny, lvl, baseowner, getting_date) VALUES(?, ? , ?, ?, ?, ?, ?);");
            statement.setInt(1, 3);
            statement.setInt(2, 1);
            statement.setString(3, "male");
            statement.setBoolean(4, true);
            statement.setInt(5, 28);
            statement.setInt(6, 3);
            statement.setDate(7, java.sql.Date.valueOf("2023-01-01"));
            statement.executeUpdate();

            statement = connection.prepareStatement("INSERT INTO exchange(idpkm1, iduser1) VALUES(?, ?);");
            statement.setInt(1, 1);
            statement.setInt(2, 1);
            statement.executeUpdate();

            statement = connection.prepareStatement("INSERT INTO exchange(idpkm1, iduser1, IDDATA_PKM2, iduser2) VALUES(?, ?, ? , ?);");
            statement.setInt(1, 1);
            statement.setInt(2, 1);
            statement.setInt(3, 4);
            statement.setInt(4, 2);
            statement.executeUpdate();

            statement = connection.prepareStatement("INSERT INTO exchange(idpkm1, iduser1, shiny) VALUES(?, ?,?);");
            statement.setInt(1, 3);
            statement.setInt(2, 2);
            statement.setBoolean(3, true);
            statement.executeUpdate();

        } catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException("could not create database !");
        }

        //Lance timer qui met a jour les 5 lvl up des joueurs toute les 24h
        Scheduler schedul = new Scheduler();
        schedul.start();
    }
}
