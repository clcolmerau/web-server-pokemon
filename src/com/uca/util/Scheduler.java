package com.uca.util;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.uca.core.UserCore;

//Met a jour les 5 lvl up des utilisateur toute les 24h
public class Scheduler {
    private ScheduledExecutorService scheduler;

    public Scheduler() {
        this.scheduler = Executors.newSingleThreadScheduledExecutor();
    }

    public void start() 
    {
        this.scheduler.scheduleAtFixedRate(() -> {
            resetLvlUp();
        },0, 24, TimeUnit.HOURS); 
    }

    private void resetLvlUp() {
        UserCore.updateLvlUp();
    }
}