<#ftl encoding="utf-8">
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/style.css" media="screen">

<title>${user.pseudo}</title>
</head>

<body xmlns="http://www.w3.org/1999/html">

<ul class = pkList>
    <#list pokemons as pokemon>
        <li class = pkListItem>
        	<img src = ${(pokemon.sprite)!} alt = ${(pokemon.name)!}</img> <br> 
        	<h3>${(pokemon.name)!}</h3>
        	<p>${(pokemon.lvl)!}</p>
        	<form action = "/lvlup" method = "POST">        
        		<input type="hidden" name="userid" value="${(user.login)!}">		
        		<button class="btn-own" name = "dataid" value = "${(pokemon.dataId)!}">Lvl up</button>
        	</form>
        	<form action = "/user/${(user.login)}/pokemon/${(pokemon.dataId)!}/exchange" method = "GET">
        	<button>Exchange</button>
        	</form>
        </li>
    </#list>
</ul>

<div class = profil>
	<div class = left>
	<h2>My profile</h2>
	<p>
		${(user.login)!} - ${(user.pseudo)!}<br>
	</p>

	<h3>Number of pokemon : ${(userinfo.pkm)!}</h3>

	<h3>Number of distinct pokemon : ${(userinfo.distinctPkm)!}</h3>

	<h3>Number of shiny pokemon : ${(userinfo.shiny)!}</h3>
	</div>

	<nav>
		<ul>
			<li>
				<a href = "/user/${(user.login)!}/exchangeIn">My incoming exchanges</a>
			</li>
			<li>
				<a href = "/user/${(user.login)!}/exchangeOut">My outcoming exchanges</a>
			</li>
			<li>
				<h4>User search</h4>
				<form id="searchForm" action = "/userSearch" method = "GET">      		
        			<input type="text" name="search" placeholder="Enter the id or pseudo"> 
        			<button type="submit" form="searchForm">Search</button>
        		</form>
			</li>
		</ul>
	</nav>
</div>

</body>
</html>


