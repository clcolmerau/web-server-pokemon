<#ftl encoding="utf-8">

<body xmlns="http://www.w3.org/1999/html">

<ul>
    <#list users as user>
        <li>
        	<h3>${(user.login)!} - ${(user.pseudo)!} </h3>
        	<form action = "/user/${user.login}" method = "GET">
        		<button>Go</button>
        	</form>
        </li>
    </#list>
</ul>

</body>

</html>
