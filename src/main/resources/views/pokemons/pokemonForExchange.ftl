<#ftl encoding="utf-8">
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/style.css" media="screen">

<title></title>
</head>
<body xmlns="http://www.w3.org/1999/html">

<ul class = pkList>
    <#list pokemons as pokemon>
        <li class = pkListItem>
        	<img src = ${(pokemon.sprite)!} alt = ${(pokemon.name)!}</img> <br> 
        	<h3>${(pokemon.name)!}</h3>
        	<p>${(pokemon.lvl)!}</p>
        	<form action = "/exchange/${(exchange)!}/offer" method = "POST">      		
        		<input type="hidden" name="exchangeId" value="${(exchange)!}"> 
        		<button name = "dataId" value = "${(pokemon.dataId)!}">Select</button>
        	</form>
        </li>
    </#list>
</ul>

</body>

</html>
