<#ftl encoding="utf-8">
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/style.css" media="screen">

<title></title>
</head>
<body xmlns="http://www.w3.org/1999/html">

<ul>
    <#list exchanges as exchange>

        <li class = exchange>
        <div class = pokemon>
        	<p>From ${(exchange.user1.pseudo)!} </p>
        	<img src = ${(exchange.pkm1.sprite)!} alt = ${(exchange.pkm1.name)!}</img> 
        	<h3>${(exchange.pkm1.name)!}</h3>
        	<p>${(exchange.pkm1.lvl)!}</p>
        </div>
        <div class = "pokemon">
        		<p>Your pokemon </p>
		    	<img src = ${(exchange.dataPkm2.sprite)!} alt = ${(exchange.dataPkm2.name)!}</img>
		    	<h3>${(exchange.dataPkm2.name)!}</h3>
		    	<p>${(exchange.dataPkm2.lvl)!}</p>
		</div>
		<div>

		   	<form action = "/exchange/${(exchange.id)!}/accepted" method = "POST"> 		
        		<button>Accept</button>
        	</form>
        	<form action = "/exchange/${(exchange.id)!}/refused" method = "POST"> 	
        		<button>Refuse</button>
        	</form>
		</div>
        </li>
    </#list>
</ul>

</body>

</html>
