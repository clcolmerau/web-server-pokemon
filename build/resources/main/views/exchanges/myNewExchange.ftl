<#ftl encoding="utf-8">
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/style.css" media="screen">

<title></title>
</head>
<body xmlns="http://www.w3.org/1999/html">


<div class = exchange>
        <div class = pokemon>
        	<p>From you </p>
        	<img src = ${(pokemon.sprite)!} alt = ${(pokemon.name)!}</img> 
        	<h3>${(pokemon.name)!}</h3>
        	<p>${(pokemon.lvl)!}</p>
        </div>
        <div>
        	<form action = "/user/${user.login}/exchangeOut/new" method = "POST" >
		    	<p>Pokemon</p>
		    	<div class = exchangeForm>
		    	<input type="text" name="pkmName" placeholder = "Enter the name of the pokemon you want"> 
		    	<input type="checkbox" name="anyPkm" value = "true"> 
		    	<p>Any</p>
		    	</div>
		    	
		    	<p>Pokemon level</p>
		    	<div class = exchangeForm>
		    	<input type="text" name="pkmLvl" placeholder = "Enter the minimum level of the pokemon you want"> 
		    	
		    	<input type="checkbox" name="anyLvl" value = "true"> 
		    	<p>Any</p>
		    	</div>
		    	
		   
		    	<p>Shiny</p>
		    	<input type="checkbox" name="pkmShiny" value = "true"> 
        	
        </div>
        <div>
        		<input type="hidden" name="dataId" value = "${(pokemon.dataId)!}">
        		<input type="hidden" name="userId" value = "${(user.login)!}">
        		<button>Post</button>
        </div>
			</form>
</div>

</body>

</html>
