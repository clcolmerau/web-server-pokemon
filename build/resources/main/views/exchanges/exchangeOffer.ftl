<#ftl encoding="utf-8">
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/style.css" media="screen">

<title></title>
</head>
<body xmlns="http://www.w3.org/1999/html">


<div class = exchange>
        <div class = pokemon>
        	<p>By ${(exchange.user1.pseudo)!} </p>
        	<img src = ${(exchange.pkm1.sprite)!} alt = ${(exchange.pkm1.name)!}</img> 
        	<h3>${(exchange.pkm1.name)!}</h3>
        	<p>${(exchange.pkm1.lvl)!}</p>
        </div>
        <div>
        	<#if exchange.dataPkm2?has_content>
		    	<img src = ${(exchange.dataPkm2.sprite)!} alt = ${(exchange.dataPkm2.name)!}</img>
		    	<h3>${(exchange.dataPkm2.name)!}</h3>
		    	<p>${(exchange.dataPkm2.lvl)!}</p>
        	<#else>
        	<form action = "/exchange/${(exchange.id)!}/selectPokemon" method = "GET" >
		    	<#if exchange.idPkm2?? && exchange.idPkm2.id == 0>
		    		<input type="hidden" name="pkmId" value="0"> 
		    		<img src = "/pokemonSprite/0.png" alt = "Any pokemon"</img> <br> 
		    	<#else>
		    		<input type="hidden" name="pkmId" value="${exchange.idPkm2.id}"> 
					<img src = "${(exchange.idPkm2.sprite)!}" alt = "${(exchange.idPkm2.name)!}"</img>
		    	</#if>
		    	<input type="hidden" name="pkmShiny" value="${exchange.idPkm2.shiny?c}"> 
		    	<#if exchange.idPkm2?? && exchange.idPkm2.name?has_content>
		    	
		    		<#if exchange.idPkm2.shiny>      
		    			<h3>Shiny ${(exchange.idPkm2.name)!}</h3>
		    		<#else>
		    			<h3>${(exchange.idPkm2.name)!}</h3>
		    		</#if>
		    	<#else>
		    		<h3>Any pokemon</h3>
		    	</#if>
		    	<#if exchange.idPkm2?? && exchange.idPkm2.lvl == 0>
		    		<input type="hidden" name="pkmLvl" value="0"> 
		    		<h3>Any level</h3>
		    	<#else>
		    		<input type="hidden" name="pkmLvl" value="${exchange.idPkm2.lvl}"> 
		    		<h3>At least level ${(exchange.idPkm2.lvl)!}</h3>
		    	</#if>
		    	<button name = "exchangeId" value = "${(exchange.id)!}">Choose</button>
        	</form>
        	</#if>
        	
        </div>
		   	<form action = "/exchange/${(exchange.id)!}/accepted" method = "POST"> 
		   	<input type="hidden" name="dataId" value="${exchange.dataPkm2.dataId}">   		
        		<button>Accept</button>
			</form>
</div>

</body>

</html>
