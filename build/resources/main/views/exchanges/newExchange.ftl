<#ftl encoding="utf-8">
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/style.css" media="screen">

<title></title>
</head>
<body xmlns="http://www.w3.org/1999/html">


<div class = exchange>
	<div class = pokemon>
		<p>From ${(exchange.user1.pseudo)!} </p>
		<img src = ${(exchange.pkm1.sprite)!} alt = ${(pokemon.name)!}</img> 
		<h3>${(exchange.pkm1.name)!}</h3>
		<p>${(exchange.pkm1.lvl)!}</p>
	</div>
	<div>
		<#if exchange.dataPkm2?has_content>
			<img src = ${(exchange.dataPkm2.sprite)!} alt = ${(exchange.dataPkm2.name)!}</img>
			<h3>${(exchange.dataPkm2.name)!}</h3>
			<p>${(exchange.dataPkm2.lvl)!}</p>
		<#else>
			<form action = "/user/${exchange.user1.login}/pokemon/${exchange.pkm1.dataId}/exchange/choosePkm" method = "GET" >
				<img src = "/pokemonSprite/0.png" alt = "Any pokemon"</img> <br> 
				<h3>Any pokemon</h3>
				<h3>Any level</h3>

				<button>Choose</button>
			</form>
		</#if>
	</div>
	<div>
		<form action = "/user/${exchange.user1.login}/pokemon/${exchange.pkm1.dataId}/exchange/valided/${(exchange.dataPkm2.dataId)!}" method = "POST"> 

			<button>Post</button>
		</form>
	</div>
</div>

</body>

</html>
