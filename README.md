# Web server Pokemon 

## How to compile 
./gradlew build

## How to run 
./gradlew run 
(it take some time)

## Initial Database

### User
<table>
	<thead>
		<tr>
			<th><ins>id</ins> : int</th>
			<th>pseudo : varchar(100)</th>
			<th>email : varchar(100)</th>
			<th>salt : varchar(50)</th>
			<th>hashpswd : varchar(100)</th>
			<th>avatarurl : varchar(100)</th>
			<th>last_date_co : date</th>
			<th>lvlup : int</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>1</td>
			<td>foo</td>
			<td>foomail</td>
			<td>$2a$10$gnIXFJepT36xAtvKa9TK1u</td>
			<td>$2a$10$gnIXFJepT36xAtvKa9TK1uSn9TEG.yABremxi6u0Tl4DBH37a0Sca</td>
			<td>null</td>
			<td>null</td>
			<td>5</td>
		</tr>
		<tr>
			<td>2</td>
			<td>toto</td>
			<td>totomail</td>
			<td>$2a$10$.QaD/yuakxw.0AiKUsjlxe</td>
			<td>$2a$10$.QaD/yuakxw.0AiKUsjlxeGnNT8vtrOB4.l.rKioBQ3ItRkSLwFZa</td>
			<td>null</td>
			<td>null</td>
			<td>5</td>
		</tr>
		<tr>
			<td>3</td>
			<td>titi</td>
			<td>titimail</td>
			<td>:$2a$10$r0XixiJmA3Z9NDPv4zJrdO</td>
			<td>$2a$10$r0XixiJmA3Z9NDPv4zJrdO05MJjwODUzMU6IfjkdbJavsvfIaBJKm</td>
			<td>null</td>
			<td>null</td>
			<td>5</td>
		</tr>
	</tbody>
</table>

### Own
<table>
	<thead>
			<tr>
			<th><ins>id</ins> : int</th>
			<th><div style="border-bottom: 1px dotted;">idowner </div>: int</th>
			<th>idpkm : int</th>
			<th>gender: varchar(20)</th>
			<th>shiny : boolean</th>
			<th>lvl :int</th>
			<th><div style="border-bottom: 1px dotted;">baseowner</div> : int</th>
			<th>getting_date : date</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>1</td>
			<td>1</td>
			<td>431</td>
			<td>"female"</td>
			<td>true</td>
			<td>23</td>
			<td>1</td>
			<td>11/11/22</td>
		</tr>
		<tr>
			<td>2</td>
			<td>1</td>
			<td>20</td>
			<td>"male"</td>
			<td>false</td>
			<td>10</td>
			<td>1</td>
			<td>10/11/22</td>
		</tr>
		<tr>
			<td>3</td>
			<td>2</td>
			<td>719</td>
			<td>"genderless"</td>
			<td>false</td>
			<td>13</td>
			<td>2</td>
			<td>08/09/22</td>
		</tr>
		<tr>
			<td>4</td>
			<td>2</td>
			<td>745</td>
			<td>"female"</td>
			<td>false</td>
			<td>32</td>
			<td>3</td>
			<td>08/09/22</td>
		</tr>
		<tr>
			<td>5</td>
			<td>3</td>
			<td>14</td>
			<td>"male"</td>
			<td>false</td>
			<td>85</td>
			<td>2</td>
			<td>08/09/22</td>
		</tr>
		<tr>
			<td>6</td>
			<td>3</td>
			<td>1</td>
			<td>"male"</td>
			<td>true</td>
			<td>28</td>
			<td>3</td>
			<td>01/01/23</td>
		</tr>
	</tbody>
</table>

### Exchange
<table>
	<thead>
		<tr>
			<th><ins>id</ins> : int</th>
			<th>idpkm1 : int</th>
			<th>idpkm2 : int</th>
			<th>iddata_pkm2: varchar(20)</th>
			<th>shiny : boolean</th>
			<th>lvl :int</th>
			<th><div style="border-bottom: 1px dotted;">iduser1</div> : int</th>
			<th>iduser2 : int</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>1</td>
			<td>1</td>
			<td>0</td>
			<td>0</td>
			<td>false</td>
			<td>0</td>
			<td>1</td>
			<td>0</td>
		</tr>
		<tr>
			<td>2</td>
			<td>1</td>
			<td>0</td>
			<td>4</td>
			<td>false</td>
			<td>0</td>
			<td>1</td>
			<td>2</td>
		</tr>
		<tr>
			<td>3</td>
			<td>3</td>
			<td>0</td>
			<td>0</td>
			<td>true</td>
			<td>0</td>
			<td>2</td>
			<td>0</td>
		</tr>
	</tbody>
</table>

### How to reset db ?
1. Remove text.mv.db
2. Décomenter le code dans src/com/uca/dao/_Initializer.java
3. ./gradlew run
4. Il est conseillé de recommenter le code 